/* Codigo com a biblioteca convert.h
 *
 *
 *
 *
 * Autor: Matheus Dall olmo
 * Autor: Pablo Gabriel Sustisso
 * Autor: Pedro Henrique Câmara
 * Autor: Samuel Grontoski
 * Data: 09/06/2022
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "convert.h"

#define SIZE 8

int main(void)
{
    int bin[SIZE], i, dec, opcao, num;

    printf("Escolha uma opcao: (1 - binario p/ decimal) ou (2 - decimal p/ binario): ");
    scanf("%d", &opcao);

    switch (opcao)
    {
    case 1:
        for (i = SIZE - 1; i >= 0; i--)
        {
            printf("Digite o valor da posicao %d\n", i);
            scanf("%d", &bin[i]);
            if ((bin[i]) < 0 || (bin[i]) > 1)
            {
                printf("Valor incorreto, binário [0,1].\n");
                i++;
            }
        }

        printf("\nNumero binario: ");
        for (i = SIZE - 1; i >= 0; i--)
        {
            printf("%d", bin[i]);
        }

        bin2dec(bin, SIZE);
        break;

    case 2:
        printf("Digite o numero (base decimal) para ser convertido: ");
        scanf("%d", &num);

        printf("\nDecimal: ");
        dec2bin(num);

    default:
        break;
    }

    return 0;
}
