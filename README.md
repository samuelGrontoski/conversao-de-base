# Algoritmo para Conversão de Base - Binário e Decimal

## Descrição

Este algoritmo converte valores binários para decimal, assim como decimal para binário.

## Autores
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11385662/avatar.png?width=100)  | Pablo Sustisso | [@PabloSustisso](https://gitlab.com/PabloSustisso) | [pablogabrielsustisso0@gmail.com](mailto:pablogabrielsustisso0@gmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11329935/avatar.png?width=100)  | Pedro Henrique Câmara | [@PedroHBCamara](https://gitlab.com/PedroHBCamara) | [pedrohenriquecamara@outlook.com.br](mailto:pedrohenriquecamara@outlook.com.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11385060/avatar.png?width=100)  | Matheus Dall olmo | [@matheusdallolmo](https://gitlab.com/matheusdallolmo) | [matheusdll@outlook.com.br](mailto:matheusdll@outlook.com.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11253360/avatar.png?width=100)  | Samuel Grontoski | [@samuelGrontoski](https://gitlab.com/samuelGrontoski) | [contatosamuelgrontoski@gmail.com](mailto:contatosamuelgrontoski@gmail.com)


