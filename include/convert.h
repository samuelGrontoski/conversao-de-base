void bin2dec(int bin[], int tam)
{
    int i, dec, soma=0;

    for(i=tam-1; i>=0; i--)
    {
        dec = bin[i] * pow(2, i);
        soma += dec;
    }

    printf("\nDecimal: %d\n\n", soma);
}

void dec2bin(int num)
{
    int i, dec[8];

    for(i=7; i>=0; i--)
    {
        if(num % 2 == 0)
        {
            dec[i] = 0;
            num = num / 2;
        }
        else
        {
            dec[i] = 1;
            num = num / 2;
        }
    }

    for (i = 0; i <= 7; i++) 
    {
        printf("%d", dec[i]);
    }
}
